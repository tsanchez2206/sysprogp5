/* Definicion del tipo de datos vector3D */
typedef struct {
	float x;
	float y;
	float z;
} vector3D;

float dotproduct(vector3D vector1, vector3D vector2);
vector3D crossproduct(vector3D vector1, vector3D vector2);
float magnitud(vector3D vector1);
int esOrtogonal(vector3D vector1, vector3D vector2);
