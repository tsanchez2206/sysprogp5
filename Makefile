OBJS = -lm -I./include

# Target que se ejecutará por defecto con el comando make
all: dynamic

# Target que compilará el proyecto usando librerías estáticas
static:
	gcc -c ./lib/vector.c -o vector.o $(OBJS)
	ar rcs libvector.a vector.o
	gcc -c ./src/main.c -o main.o $(OBJS)
	gcc -c ./src/main.c $(OBJS)
	gcc -static -o ejecutableEs main.o ./libvector.a $(OBJS)

# Target que compilará el proyecto usando librerías dinámicas
dynamic: 
	gcc -fPIC -c ./lib/vector.c -o vector.o $(OBJS)
	gcc -shared -fPIC -o libvector.so vector.o
	gcc -o ejecutableDi ./src/main.c ./libvector.so $(OBJS)	
	
# Limpiar archivos temporales
clean:
	rm -f ejecutableDi *.o *.so
	rm -f ejecutableEs *.o *.a 
