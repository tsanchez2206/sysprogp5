/* implementar aquí las funciones requeridas */
#include<stdio.h>
#include<math.h>
#include<libvector.h>

float dotproduct(vector3D vector1,vector3D vector2)
{
	float prodEscalar;
	float px,py,pz;
	px=vector1.x*vector2.x;
	py=vector1.y*vector2.y;	
	pz=vector1.z*vector2.z;
	prodEscalar=px+py+pz;
	return prodEscalar;
}


vector3D crossproduct(vector3D vector1,vector3D vector2)
{
        vector3D producto;
	producto.x=(vector1.y*vector2.z)-(vector1.z*vector2.y);
	producto.y=(vector1.z*vector2.x)-(vector1.x*vector2.z);
	producto.z=(vector1.x*vector2.y)-(vector1.y*vector2.x);
	printf("Producto vectorial: %.2fí %.2fj %.2fḱ\n",producto.x,producto.y,producto.z); 
	return producto;
	  
       
}

float magnitud(vector3D vector1)
{
	float magni,cuaX,cuaY,cuaZ;
	cuaX=vector1.x*vector1.x;	
	cuaY=vector1.y*vector1.y;
	cuaZ=vector1.z*vector1.z;
	magni=cuaX+cuaY+cuaZ;
	return magni;

}

int esOrtogonal(vector3D vector1, vector3D vector2)
{
	int ortogonal;
	ortogonal=(vector1.x*vector2.x)+(vector1.y*vector2.y)+(vector1.z*vector2.z);
	if(ortogonal==0)
	{return 1;}
	else
	{return 0;}
	
}


