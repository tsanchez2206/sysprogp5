#include<stdio.h>
#include<math.h>
#include<libvector.h>

int main()
{
	double proEscalar,magnitudV1,magnitudV2;
	int ortogonal;
	
	vector3D vector1,vector2,proVectorial;
	//INGRESO DE LAS COORDENADAS DEL PRIMER VECTOR
	printf("Ingrese las coordenadas del vector 1\n");
	printf("Ingrese la coordenada x1: ");
	scanf("%f",&vector1.x);
	printf("Ingrese la coordenada y1: ");
	scanf("%f",&vector1.y );
	printf("Ingrese la coordenada z1: ");
	scanf("%f",&vector1.z );
	//INGRESO DE LAS COORDENADAS DEL SEGUNDO VECTOR
	printf("-----------------------------------------\n");
	printf("Ingrese las coordenadas del vector 2\n");
	printf("Ingrese la coordenada x2: ");
	scanf("%f",&vector2.x );
	printf("Ingrese la coordenada y2: ");
	scanf("%f",&vector2.y );
	printf("Ingrese la coordenada z2: ");
	scanf("%f",&vector2.z);
	// Producto escalar entre dos vectores
	proEscalar=dotproduct(vector1,vector2);
	printf("-----------------------------------------\n");
	printf("-------------RESULTADOS------------------\n");
	printf("El producto escalar entre ambos vectores es: %.2f\n", proEscalar);
	//Producto vectorial entre dos vectores
	proVectorial=crossproduct(vector1,vector2);
	//printf("Producto vectorial es:%.2f\n",proVectorial);
	//Magnitud de los vectores 
	magnitudV1=magnitud(vector1);
	magnitudV2=magnitud(vector2);
	printf("Magnitud del vector 1: %.2f\n",sqrt(magnitudV1));
	printf("Magnitud del vector 2: %.2f\n",sqrt(magnitudV2));
	//ES ORTOGONAL O NO
	/* 0=NO SON ORTOGONALES, 1= SON ORTOGANALES */
	ortogonal=esOrtogonal(vector1,vector2);
	printf("IMPORTANTE:-> 0=NO son ortogonales   1=SON ortogonales\n");
	printf("Respuesta de vectores ortogonales:  %d\n",ortogonal);
		
	
}
